package com.endrjudev.pixaby.domain

import kotlinx.coroutines.flow.Flow

interface ImageRepository {

    fun getImages(phrase: String): Flow<List<Image>>
    fun getImage(id: Int): Flow<Image>

    suspend fun searchImages(phrase: String)
}