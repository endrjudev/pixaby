package com.endrjudev.pixaby.domain

data class Image(
    val id: Int,
    val largeImageURL: String,
    val previewURL: String,
    val user: String,
    val tags: List<String>,
    val likes: Int,
    val comments: Int,
    val downloads: Int
)