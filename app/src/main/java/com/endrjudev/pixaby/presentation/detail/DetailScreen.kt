package com.endrjudev.pixaby.presentation.detail

import androidx.annotation.DrawableRes
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.transform.CircleCropTransformation
import com.endrjudev.pixaby.R

@Composable
fun DetailScreen(
    viewModel: DetailViewModel = hiltViewModel()
) {
    when (val state = viewModel.uiState.collectAsState().value) {
        is DetailViewModel.UiState.Error -> {
            // implement
        }
        is DetailViewModel.UiState.Loaded -> {
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.spacedBy(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                contentPadding = PaddingValues(16.dp)
            ) {
                item {
                    AsyncImage(
                        modifier = Modifier
                            .size(size = 256.dp)
                            .clip(shape = CircleShape),
                        model = ImageRequest.Builder(LocalContext.current)
                            .data(state.image.largeImageURL)
                            .crossfade(enable = true)
                            .transformations(CircleCropTransformation())
                            .build(),
                        contentScale = ContentScale.Crop,
                        contentDescription = null,

                        )
                }

                item {
                    Row(
                        horizontalArrangement = Arrangement.spacedBy(4.dp)
                    ) {
                        state.image.tags.forEach {
                            Text(
                                modifier = Modifier
                                    .border(
                                        border = BorderStroke(2.dp, color = Color.Black),
                                        shape = RoundedCornerShape(8.dp)
                                    )
                                    .padding(8.dp),
                                text = it
                            )
                        }
                    }
                }

                item {
                    Row(horizontalArrangement = Arrangement.spacedBy(8.dp)) {
                        IconWithText(
                            icon = R.drawable.ic_like,
                            text = state.image.likes.toString()
                        )
                        IconWithText(
                            icon = R.drawable.ic_comment,
                            text = state.image.comments.toString()
                        )
                        IconWithText(
                            icon = R.drawable.ic_download,
                            text = state.image.downloads.toString()
                        )
                    }
                }

                item {
                    Text(text = state.image.user)
                }
            }
        }
        is DetailViewModel.UiState.Loading -> CircularProgressIndicator()
    }
}

@Composable
private fun IconWithText(
    @DrawableRes icon: Int,
    text: String
) {
    Box(
        modifier = Modifier
            .border(
                border = BorderStroke(2.dp, color = Color.Black),
                shape = RoundedCornerShape(8.dp)
            )
            .padding(8.dp)
    ) {
        Column(
            modifier = Modifier.size(64.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Icon(
                modifier = Modifier.weight(1f),
                painter = rememberAsyncImagePainter(model = icon),
                contentDescription = null
            )
            Text(
                modifier = Modifier.weight(1f),
                text = text,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}