@file:OptIn(ExperimentalMaterial3Api::class)

package com.endrjudev.pixaby.presentation.search

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.endrjudev.pixaby.domain.Image
import com.endrjudev.pixaby.presentation.detail.DetailsDestination

@Composable
fun SearchScreen(viewModel: SearchViewModel = hiltViewModel()) {
    val state = viewModel.uiState.collectAsState().value

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    TextField(
                        modifier = Modifier.fillMaxWidth(),
                        value = state.phrase,
                        placeholder = {
                            Text(text = "Search for anything...")
                        },
                        trailingIcon = {
                            Icon(
                                imageVector = Icons.Default.Search,
                                contentDescription = null
                            )
                        },
                        colors = TextFieldDefaults.textFieldColors(
                            focusedIndicatorColor = Color.Transparent,
                            unfocusedIndicatorColor = Color.Transparent,
                            containerColor = Color.Transparent
                        ),
                        onValueChange = viewModel::search
                    )
                }
            )
        },
        content = {
            SearchContent(it, state) { image ->
                viewModel.navigate(DetailsDestination.createDetailsRoute(image))
            }
        }
    )
}

@Composable
fun SearchContent(
    contentPadding: PaddingValues,
    state: SearchViewModel.UiState,
    onItemClick: (Image) -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(contentPadding),
        contentAlignment = Alignment.Center
    ) {
        when (state) {
            is SearchViewModel.UiState.Error -> {
                // implement
            }
            is SearchViewModel.UiState.Loaded -> {
                LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    contentPadding = PaddingValues(16.dp),
                    verticalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    items(state.images) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .clickable { onItemClick(it) },
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween
                        ) {
                            Text(text = it.user)
                            AsyncImage(
                                model = ImageRequest.Builder(LocalContext.current)
                                    .data(it.previewURL)
                                    .crossfade(true)
                                    .build(),
                                contentDescription = null,
                                contentScale = ContentScale.Crop,
                                modifier = Modifier
                                    .size(64.dp)
                                    .clip(CircleShape)
                            )
                        }
                    }
                }
            }
            is SearchViewModel.UiState.Loading -> CircularProgressIndicator()
        }
    }
}