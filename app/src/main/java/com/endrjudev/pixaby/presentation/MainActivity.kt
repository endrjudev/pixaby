package com.endrjudev.pixaby.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.LaunchedEffect
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.endrjudev.pixaby.presentation.detail.DetailScreen
import com.endrjudev.pixaby.presentation.detail.DetailsDestination
import com.endrjudev.pixaby.presentation.navigation.Navigation
import com.endrjudev.pixaby.presentation.navigation.NavigatorEvent
import com.endrjudev.pixaby.presentation.search.SearchDestination
import com.endrjudev.pixaby.presentation.search.SearchScreen
import com.endrjudev.pixaby.presentation.theme.PixabyTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var navigation: Navigation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PixabyTheme {
                val navController = rememberNavController()

                LaunchedEffect(navController) {
                    navigation.destinations
                        .flowWithLifecycle(lifecycle)
                        .onEach {
                            when (val event = it) {
                                is NavigatorEvent.NavigateTo -> navController.navigate(
                                    event.destination,
                                    event.builder
                                )
                            }
                        }
                        .flowOn(Dispatchers.Main.immediate)
                        .launchIn(lifecycleScope)
                }

                NavHost(
                    navController = navController,
                    startDestination = SearchDestination.route()
                ) {
                    composable(
                        route = SearchDestination.route(),
                        arguments = SearchDestination.arguments
                    ) {
                        SearchScreen()
                    }
                    composable(
                        route = DetailsDestination.route(),
                        arguments = DetailsDestination.arguments
                    ) {
                        DetailScreen()
                    }
                }
            }
        }
    }
}