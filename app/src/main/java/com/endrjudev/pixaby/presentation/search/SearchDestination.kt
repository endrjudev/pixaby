package com.endrjudev.pixaby.presentation.search

import com.endrjudev.pixaby.presentation.Destination

object SearchDestination : Destination {
    override fun route(): String = "images"
}