package com.endrjudev.pixaby.presentation

import com.endrjudev.pixaby.presentation.navigation.Navigation
import com.endrjudev.pixaby.presentation.navigation.NavigationImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PresentationModule {

    @Provides
    @Singleton
    fun provideNavigation(): Navigation = NavigationImpl()
}