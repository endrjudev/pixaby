package com.endrjudev.pixaby.presentation.navigation

import androidx.navigation.NavOptionsBuilder
import kotlinx.coroutines.flow.Flow

interface Navigation {

    val destinations: Flow<NavigatorEvent>

    fun navigate(route: String, builder: NavOptionsBuilder.() -> Unit = { launchSingleTop = true })
}