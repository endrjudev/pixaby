package com.endrjudev.pixaby.presentation.detail

import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.endrjudev.pixaby.domain.Image
import com.endrjudev.pixaby.presentation.Destination

object DetailsDestination : Destination {

    override fun route(): String = "images/{id}"

    override val arguments: List<NamedNavArgument>
        get() = listOf(
            navArgument("id") { type = NavType.IntType }
        )

    fun createDetailsRoute(image: Image) = "images/${image.id}"
}