package com.endrjudev.pixaby.presentation

import androidx.navigation.NamedNavArgument

fun interface Destination {

    fun route(): String
    val arguments: List<NamedNavArgument>
        get() = emptyList()
}