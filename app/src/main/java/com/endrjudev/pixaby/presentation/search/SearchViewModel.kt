package com.endrjudev.pixaby.presentation.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.endrjudev.pixaby.domain.Image
import com.endrjudev.pixaby.domain.ImageRepository
import com.endrjudev.pixaby.presentation.navigation.Navigation
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val imageRepository: ImageRepository,
    private val navigator: Navigation
) : ViewModel(), Navigation by navigator {

    private val phraseState = MutableStateFlow(INITIAL_SEARCH_PHRASE)
    private val searchState = phraseState.flatMapLatest { imageRepository.getImages(it) }

    val uiState = combine<String, List<Image>, UiState>(
        phraseState,
        searchState
    ) { phrase, images ->
        UiState.Loaded(
            phrase = phrase,
            images = images
        )
    }
        .catch {
            Timber.e(it)
            emit(UiState.Error(phraseState.value))
        }
        .flowOn(Dispatchers.IO)
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(DEFAULT_TIMEOUT),
            initialValue = UiState.Loading(phraseState.value)
        )

    init {
        phraseState
            .debounce(DEFAULT_DEBOUNCE)
            .onEach { imageRepository.searchImages(it) }
            .launchIn(viewModelScope)
    }

    fun search(phrase: String) {
        phraseState.update {
            phrase
        }
    }

    sealed class UiState {

        abstract val phrase: String

        data class Loaded(
            val images: List<Image>,
            override val phrase: String
        ) : UiState()

        data class Loading(override val phrase: String) : UiState()

        data class Error(override val phrase: String) : UiState()
    }

    private companion object {
        const val INITIAL_SEARCH_PHRASE = "fruits"
        const val DEFAULT_DEBOUNCE = 500L
        const val DEFAULT_TIMEOUT = 5000L
    }
}