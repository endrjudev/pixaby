package com.endrjudev.pixaby.presentation.navigation

import androidx.navigation.NavOptionsBuilder

sealed interface NavigatorEvent {
    data class NavigateTo(
        val destination: String,
        val builder: NavOptionsBuilder.() -> Unit
    ) : NavigatorEvent
}