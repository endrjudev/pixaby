package com.endrjudev.pixaby.presentation.detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.endrjudev.pixaby.domain.Image
import com.endrjudev.pixaby.domain.ImageRepository
import com.endrjudev.pixaby.presentation.navigation.Navigation
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    imageRepository: ImageRepository,
    private val navigation: Navigation
) : ViewModel(), Navigation by navigation {

    private val imageId = savedStateHandle.get<Int>("id") ?: error("Image id cannot be null")

    val uiState = imageRepository.getImage(imageId)
        .map<Image, UiState> { UiState.Loaded(it) }
        .catch { emit(UiState.Error) }
        .stateIn(
            viewModelScope,
            SharingStarted.WhileSubscribed(5000L),
            initialValue = UiState.Loading
        )

    sealed class UiState {

        data class Loaded(val image: Image) : UiState()
        data object Error : UiState()
        data object Loading : UiState()
    }
}