package com.endrjudev.pixaby.presentation.navigation

import androidx.navigation.NavOptionsBuilder
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow

class NavigationImpl : Navigation {

    private val navigationEvents = Channel<NavigatorEvent>()

    override val destinations = navigationEvents.receiveAsFlow()

    override fun navigate(
        route: String,
        builder: NavOptionsBuilder.() -> Unit
    ) {
        navigationEvents.trySend(NavigatorEvent.NavigateTo(route, builder))
    }
}