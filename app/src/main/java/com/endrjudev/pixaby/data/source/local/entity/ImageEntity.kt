package com.endrjudev.pixaby.data.source.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.serialization.SerialName

@Entity(
    tableName = "image",
    primaryKeys = [
        "id",
        "searchPhrase"
    ]
)
data class ImageEntity(
    @SerialName("imageId")
    val id: Int,
    @SerialName("largeImageURL")
    val largeImageURL: String,
    @SerialName("previewURL")
    val previewURL: String,
    @SerialName("user")
    val user: String,
    @SerialName("tags")
    val tags: String,
    @SerialName("likes")
    val likes: Int,
    @SerialName("comments")
    val comments: Int,
    @SerialName("downloads")
    val downloads: Int,
    @SerialName("searchPhrase")
    val searchPhrase: String
)