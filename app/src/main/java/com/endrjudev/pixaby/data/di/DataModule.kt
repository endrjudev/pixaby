package com.endrjudev.pixaby.data.di

import android.content.Context
import androidx.room.Room
import com.endrjudev.pixaby.BuildConfig
import com.endrjudev.pixaby.data.repository.ImageRepositoryImpl
import com.endrjudev.pixaby.data.source.local.LocalDataSource
import com.endrjudev.pixaby.data.source.local.database.ImageDatabase
import com.endrjudev.pixaby.data.source.remote.RemoteDataSource
import com.endrjudev.pixaby.domain.ImageRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.cache.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import timber.log.Timber
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): ImageDatabase = Room.databaseBuilder(
        context,
        ImageDatabase::class.java,
        "image.db"
    ).build()

    @Provides
    @Singleton
    fun provideHttpClient(): HttpClient = HttpClient(Android) {
        install(Logging) {
            level = LogLevel.ALL
            logger = object : Logger {
                override fun log(message: String) {
                    Timber.i(message)
                }
            }
        }
        install(ContentNegotiation) {
            json()
        }
        install(DefaultRequest) {
            url("https://pixabay.com/api/") {
                protocol = URLProtocol.HTTPS
                host = "pixabay.com"
                path("api/")
                parameters.append(
                    "key", BuildConfig.API_KEY
                )
            }
        }
        install(HttpCache)
    }

    @Provides
    @Singleton
    fun provideImageRepository(
        remoteDataSource: RemoteDataSource,
        localDataSource: LocalDataSource
    ): ImageRepository = ImageRepositoryImpl(remoteDataSource, localDataSource)
}