package com.endrjudev.pixaby.data.source.local.database

import androidx.room.*
import com.endrjudev.pixaby.data.source.local.entity.ImageEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ImageDao {

    @Query("SELECT * from image WHERE :phrase LIKE searchPhrase")
    fun getImagesWithPhrase(phrase: String): Flow<List<ImageEntity>>

    @Query("SELECT * from image WHERE :id == id LIMIT 1")
    fun getImageWithId(id: Int): Flow<ImageEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertImages(images: List<ImageEntity>)
}