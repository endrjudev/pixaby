package com.endrjudev.pixaby.data.source.remote

import com.endrjudev.pixaby.data.source.remote.model.ImagesResponse
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteDataSource @Inject constructor(
    private val httpClient: HttpClient
) {

    suspend fun getImages(phrase: String): ImagesResponse {
        val response = httpClient.get {
            parameter("q", phrase)
        }
        return response.body()
    }
}