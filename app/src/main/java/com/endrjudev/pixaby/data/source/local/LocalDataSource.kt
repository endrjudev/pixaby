package com.endrjudev.pixaby.data.source.local

import com.endrjudev.pixaby.data.source.local.database.ImageDatabase
import com.endrjudev.pixaby.data.source.local.entity.ImageEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalDataSource @Inject constructor(
    private val imageDatabase: ImageDatabase
) {

    suspend fun saveImages(images: List<ImageEntity>) =
        imageDatabase.imageDao().insertImages(images)

    fun getImages(phrase: String) = imageDatabase.imageDao().getImagesWithPhrase(phrase)

    fun getImage(id: Int) = imageDatabase.imageDao().getImageWithId(id)
}