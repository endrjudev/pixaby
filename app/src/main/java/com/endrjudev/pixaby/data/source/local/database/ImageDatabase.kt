package com.endrjudev.pixaby.data.source.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.endrjudev.pixaby.data.source.local.entity.ImageEntity

@Database(
    entities = [
        ImageEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class ImageDatabase : RoomDatabase() {

    abstract fun imageDao(): ImageDao
}