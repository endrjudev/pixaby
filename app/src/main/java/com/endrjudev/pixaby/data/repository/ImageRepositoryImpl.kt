package com.endrjudev.pixaby.data.repository

import com.endrjudev.pixaby.data.source.local.LocalDataSource
import com.endrjudev.pixaby.data.source.local.entity.ImageEntity
import com.endrjudev.pixaby.data.source.remote.RemoteDataSource
import com.endrjudev.pixaby.domain.Image
import com.endrjudev.pixaby.domain.ImageRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ImageRepositoryImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) : ImageRepository {

    override fun getImage(id: Int): Flow<Image> =
        localDataSource.getImage(id).map { entity ->
            Image(
                id = entity.id,
                largeImageURL = entity.largeImageURL,
                previewURL = entity.previewURL,
                user = entity.user,
                tags = entity.tags.split(","),
                likes = entity.likes,
                comments = entity.comments,
                downloads = entity.downloads
            )
        }

    override fun getImages(phrase: String): Flow<List<Image>> =
        localDataSource.getImages(phrase = phrase).map { entityList ->
            entityList.map { entity ->
                Image(
                    id = entity.id,
                    largeImageURL = entity.largeImageURL,
                    previewURL = entity.previewURL,
                    user = entity.user,
                    tags = entity.tags.split(","),
                    likes = entity.likes,
                    comments = entity.comments,
                    downloads = entity.downloads
                )
            }
        }

    override suspend fun searchImages(phrase: String) {
        val response = remoteDataSource.getImages(phrase)
        localDataSource.saveImages(
            response.hits.map { hit ->
                ImageEntity(
                    id = hit.id,
                    largeImageURL = hit.largeImageURL,
                    previewURL = hit.previewURL,
                    user = hit.user,
                    tags = hit.tags,
                    likes = hit.likes,
                    comments = hit.comments,
                    downloads = hit.downloads,
                    searchPhrase = phrase
                )
            }
        )
    }
}